package com.example.administrator.visitor;

import com.orm.SugarRecord;
import com.orm.dsl.Table;



/**
 * Created by Administrator on 9/4/2017.
 */
@Table(name="registration")

public class Db_registration extends SugarRecord {
    String name;
    String title;
    String email_address;
    String organisation;
    String mobile_number;
    String interest;


    public Db_registration(){
    }


    public  Db_registration(String name,String title,String organisation,String email_address,String mobile_number,String interest){

        this.name=name;
        this.title=title;
        this.organisation=organisation;
        this.email_address=email_address;
        this.mobile_number=mobile_number;
        this.interest=interest;


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }
}
