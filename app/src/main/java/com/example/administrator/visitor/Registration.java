package com.example.administrator.visitor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import android.database.sqlite.SQLiteDatabase;

import com.opencsv.CSVWriter;
import com.orm.SugarContext;
import com.orm.SugarDb;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.example.administrator.visitor.R.id.all;

public class Registration extends AppCompatActivity {
    private static final String SAMPLE_DB_NAME = "visitor.db";
    private static final String SAMPLE_TABLE_NAME = "registration";
    final boolean success=true;


Button submit;
    String s_identification="";
    String s_disbursement="",s_transfer="",s_me="",s_tracking="";
    EditText name;
    EditText title;
    EditText email;
    EditText number;
    EditText Organisation;
    CheckBox box_identification,box_disbursement,box_transfer,box_me,box_tracking;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        // making the softkeyboard not to appear on launching

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ProgressDialog dialog = new ProgressDialog(Registration.this);

      final   List<Db_registration>all=dataBase.findWithQuery(Db_registration.class,"select *from registration ");
     final   int db_size1=all.size();

        name=(EditText) findViewById(R.id.txt_name);
        email=(EditText) findViewById(R.id.txt_email);
        number=(EditText) findViewById(R.id.txt_phone);
        Organisation=(EditText) findViewById(R.id.txt_organisation);
        title=(EditText) findViewById(R.id.txt_title) ;

        box_identification=(CheckBox) findViewById(R.id.b_identification);
        box_disbursement=(CheckBox) findViewById(R.id.b_disbursement);
        box_transfer=(CheckBox) findViewById(R.id.b_transfer);
        box_me=(CheckBox) findViewById(R.id.b_ie);
        box_tracking=(CheckBox) findViewById(R.id.b_tracking);


        box_me.setText("M&E");


        submit=(Button) findViewById(R.id.b_submit) ;


        box_identification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(box_identification.isChecked()){

                   s_identification= box_identification.getText().toString();
                   // Toast.makeText(Registration.this, s_identification, Toast.LENGTH_SHORT).show();
                }
                else {
                    s_identification="";
                   // Toast.makeText(Registration.this, s_identification, Toast.LENGTH_SHORT).show();

                }



            }
        });

        box_disbursement.setOnCheckedChangeListener(  new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(box_disbursement.isChecked()){

                    s_disbursement= box_disbursement.getText().toString();
                   // Toast.makeText(Registration.this, s_disbursement, Toast.LENGTH_SHORT).show();
                }
                else {
                    s_disbursement="";
                    //Toast.makeText(Registration.this, s_disbursement, Toast.LENGTH_SHORT).show();

                }



            }
        });

        box_transfer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(box_transfer.isChecked()){

                    s_transfer= box_transfer.getText().toString();
                    //Toast.makeText(Registration.this, s_transfer, Toast.LENGTH_SHORT).show();
                }
                else {
                    s_transfer="";
                    //Toast.makeText(Registration.this, s_transfer, Toast.LENGTH_SHORT).show();

                }



            }
        });

        box_me.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(box_me.isChecked()){

                    s_me= box_me.getText().toString();
                    //Toast.makeText(Registration.this, s_me, Toast.LENGTH_SHORT).show();
                }
                else {
                    s_me="";
                    //Toast.makeText(Registration.this, s_me, Toast.LENGTH_SHORT).show();

                }



            }
        });
        box_tracking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(box_tracking.isChecked()){

                    s_tracking= box_tracking.getText().toString();
                   // Toast.makeText(Registration.this, s_tracking, Toast.LENGTH_SHORT).show();
                }
                else {
                    s_tracking="";
                    //Toast.makeText(Registration.this, s_tracking, Toast.LENGTH_SHORT).show();

                }



            }
        });




        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String names=name.getText().toString();
                String emails=email.getText().toString();
                String numbers=number.getText().toString();
                String Organisations=Organisation.getText().toString();
                String titles=title.getText().toString();
                String interests=checkNull(s_identification)+  checkNull(s_disbursement)+checkNull(s_transfer)+checkNull(s_me)+checkNull(s_tracking);



                   //CHECKING FOR VALIDATIONS
                    if (TextUtils.isEmpty(names)) {
                        name.requestFocus(); //gets the cursor to name edittext
                        name.setError("Please Enter name"); //sets error message

                        return;
                    }



                    else if(TextUtils.isEmpty(titles)){
                        title.requestFocus();
                        title.setError("Please Enter Title");
                        return;
                    }

                    /*else if(!(numbers.length()==10 )){
                        //number.setBackground(number.getBackground().getConstantState().newDrawable());
                       number.requestFocus();
                        number.setError("Please Enter a valid  Number");

                        return;
                    }*/

                    else if(TextUtils.isEmpty(Organisations) ){
                        Organisation.requestFocus();
                        Organisation.setError("Please Enter Organisation");
                        return;
                    }

                    else if(TextUtils.isEmpty(emails))  {
                        email.requestFocus();
                        email.setError("Please Enter Email");
                        return;
                    }

                else if (!Patterns.EMAIL_ADDRESS.matcher(emails).matches()){
                    email.requestFocus();
                        email.setError("Please enter a Valid E-Mail Address!");
                }



                    else if(TextUtils.isEmpty(numbers)){
                        number.requestFocus();
                        number.setError("Please Enter Number");
                        return;
                    }

                    else    if(!((box_identification.isChecked()) || (box_disbursement.isChecked()) || (box_transfer.isChecked()) || (box_me.isChecked())||
                (box_tracking.isChecked()) ))
                {
                    SweetAlertDialog sweet=   new SweetAlertDialog(Registration.this, SweetAlertDialog.ERROR_TYPE);
                    sweet   .setTitleText("Oops...");
                    sweet   .setContentText("No CheckBox Choosen!");
                    sweet   .show();

                 }

                else {
                        //SAVING DATA TO DB
                        Db_registration admin = new Db_registration(names,titles,Organisations,emails,numbers,interests);
                        admin.save();

                        //CHECKING IF A NEW RECORD HAS BEEN ADDED
                        List<Db_registration>all2=dataBase.findWithQuery(Db_registration.class,"select *from registration ");
                        if(all2.size()>db_size1) {
                            SweetAlertDialog sweet = new SweetAlertDialog(Registration.this, SweetAlertDialog.SUCCESS_TYPE);
                            sweet.setTitleText("Success!!!");
                            sweet.setContentText("Saved Successfully!");
                            sweet.show();


                            //clear the edit fields and edit Texts
                            name.setText("");
                            email.setText("");
                            number.setText("");
                            Organisation.setText("");
                            title.setText("");

                            if(box_disbursement.isChecked()){
                                box_disbursement.toggle();
                            }

                            if(box_identification.isChecked()){
                              box_identification.toggle();
                            }
                            if(box_transfer.isChecked()){
                                box_transfer.toggle();
                            }
                            if(box_tracking.isChecked()){
                                box_tracking.toggle();
                            }
                            if(box_me.isChecked()){
                                box_me.toggle();
                            }



                        }
                        else if (all.size()==db_size1) {
                            SweetAlertDialog sweet = new SweetAlertDialog(Registration.this, SweetAlertDialog.ERROR_TYPE);
                            sweet.setTitleText("FAILED!!!");
                            sweet.setContentText("Saving failed!");
                            sweet.show();

                        }



                }


            }
        });




        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }



//METHOD TO CHECK IF THE BOXES WERE CHECKED SO THAT TO APPEND NEXT LINE
    private String checkNull(String given){
        if(!(given.equals(""))){

            given= given+"\n" ;

        }
        else if(given.equals(""))
        {
            given= given;
        }

        return given;


    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_visitor_login, menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

//HANDLING THE IMPORT MENU ITEM
        if (id == R.id.action_settings) {

            String currentDBPath = "/data/"+ "com.example.administrator.visitor" +"/databases/visitor.db"; //GET DB PATH
            File dbFile = getDatabasePath(""+currentDBPath);

            File exportDir = new File(Environment.getExternalStorageDirectory(), "/visitorData/"); //CREATING FOLDER

            //Log.e("########",""+exportDir.toString());

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }

            File file = new File(exportDir, "myfile.csv");

            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                Field f = null;

                try {
                    f = SugarContext.getSugarContext().getClass().getDeclaredField("sugarDb");
                    f.setAccessible(true);
                    SugarDb db = (SugarDb) f.get(SugarContext.getSugarContext());
                    Cursor curCSV = db.getDB().
                            rawQuery("Select * from registration " , null);
                    csvWrite.writeNext(curCSV.getColumnNames());

                    while(curCSV.moveToNext()) {
                        String arrStr[]=null;
                        String[] mySecondStringArray = new String[curCSV.getColumnNames().length];
                        for(int i=0;i<curCSV.getColumnNames().length;i++)
                        {
                            mySecondStringArray[i] =curCSV.getString(i);
                        }
                        csvWrite.writeNext(mySecondStringArray);
                    }



                    if (success) {

                        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Imported Successfully ")
                                .setContentText("Send Mail?")
                                .setConfirmText("Yes,Send!")

                                .setCancelText("No,Thankyou!")

                                .showCancelButton(true)
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.cancel();
                                    }
                                })
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.cancel();
                                        String filename="myfile.vcf";
                                        //File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), filename);
                                        File filelocation = new File(Environment.getExternalStorageDirectory(), "/visitorData/myfile.csv");
                                        Uri path = Uri.fromFile(filelocation);

                                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                        // setting the type to mail

                                        emailIntent .setType("vnd.android.cursor.dir/email");
                                        String to[] = {"johngikuma17@gmail.com"};
                                        emailIntent .putExtra(Intent.EXTRA_EMAIL, to);
                                        // the attachment
                                        emailIntent .putExtra(Intent.EXTRA_STREAM, path);
                                        // the mail subject
                                        emailIntent .putExtra(Intent.EXTRA_SUBJECT, "visitor Details");
                                        startActivity(Intent.createChooser(emailIntent , "Send email...")

                                        );











                                    }
                                })
                                .show();



                    } else {
                        Toast.makeText(Registration.this, "Export failed", Toast.LENGTH_SHORT).show();
                    }

                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                csvWrite.close();
                //curCSV.close();
                return true;

            } catch (IOException e) {
                Log.e("Registration", e.getMessage(), e);
                return false;
            }




        }





        return super.onOptionsItemSelected(item);
    }
}
