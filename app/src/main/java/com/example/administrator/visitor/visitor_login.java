package com.example.administrator.visitor;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.orm.SugarDb;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class visitor_login extends AppCompatActivity {
Button login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);



                  //CHECK IF THIS TABLE EXISTS
        SugarDb db = new SugarDb(visitor_login.this);
        db.onCreate(db.getDB());

        dataBase admin = new dataBase("compulynx", "admin1234");
        admin.save();
        List<dataBase> user_list  = dataBase.find(dataBase.class, "username = ?", "compulynx");



        if(user_list.size()<1) {


            Toast.makeText(visitor_login.this, "db created successfully", Toast.LENGTH_SHORT).show();
        }
        //Toast.makeText(visitor_login.this, "record size is "+user_list.size(), Toast.LENGTH_SHORT).show();

        login=(Button) findViewById(R.id.b_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText Name = (EditText) findViewById(R.id.txt_username);
                String user = Name.getText().toString();

                EditText pass = (EditText) findViewById(R.id.input_password);
                String Pass = pass.getText().toString();

                final String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};
                ActivityCompat.requestPermissions(visitor_login.this, PERMISSIONS_STORAGE, 9);

               /* final String[] PERMISSIONS_READ = {Manifest.permission.READ_EXTERNAL_STORAGE};
                ActivityCompat.requestPermissions(visitor_login.this, PERMISSIONS_READ, 9);*/




    if (TextUtils.isEmpty(user) ) {
        Name.requestFocus();
        Name.setError("Please Enter Username");
        return;
    }

   else if (TextUtils.isEmpty(Pass) ) {
        pass.requestFocus();
        pass.setError("Please Enter Password");
        return;
    }



    else {


        List<dataBase>all=dataBase.findWithQuery(dataBase.class,"select *from adminUsers where username=? AND password=?",user,Pass);

        if(all.size()<1) {


    SweetAlertDialog sweet=   new SweetAlertDialog(visitor_login.this, SweetAlertDialog.ERROR_TYPE);
         sweet   .setTitleText("Oops...");
         sweet   .setContentText("Wrong Username or Password!");
         sweet   .show();

}
        else{

            Intent intent=new Intent(visitor_login.this,Registration.class);
            startActivity(intent);
        }

}










            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_visitor_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("        Exit?    ")
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        visitor_login.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }
}
